#!/usr/bin/env sh

DOCKER_SOCKET=/var/run/docker.sock
DOCKER_GROUP=docker
USER=dg

set -e

if [ -S ${DOCKER_SOCKET} ]; then
  DOCKER_GID=$(stat -c '%g' ${DOCKER_SOCKET})
  groupmod --gid "${DOCKER_GID}" ${DOCKER_GROUP}
  usermod --append --groups ${DOCKER_GROUP} ${USER}
fi
sg ${DOCKER_GROUP} -c "ash"

if [ "$1" = "render" ]
then
  /docker-graph/src/"$*"
else
  echo "Unknown command: $1"
  exit 1
fi

exit