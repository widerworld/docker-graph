FROM ihasco/docker-graph
ARG with_xdebug
RUN test 1 -eq "$with_xdebug" && \
      apk add --no-cache --virtual .builddeps \
            gcc \
            make \
            musl-dev \
            php8-pear \
            php8-dev \
            zip && \
      pecl install xdebug && \
      docker-php-ext-enable xdebug && \
      echo "xdebug.idekey=IDE_KEY"               >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
      echo "xdebug.discover_client_host=1"       >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
      echo "xdebug.mode=debug"                   >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
      echo "xdebug.start_with_request=trigger"   >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
      apk del --no-network .builddeps && \
      rm -rf /tmp/* || :