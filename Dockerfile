FROM php:8.1-alpine
WORKDIR /docker-graph

COPY bin/docker-entrypoint.sh /docker-graph/bin/docker-entrypoint.sh
COPY src /docker-graph/src
COPY composer.json /docker-graph/composer.json
COPY composer.lock /docker-graph/composer.lock
COPY composer.phar /docker-graph/composer.phar

RUN apk update && \
    apk add --no-cache \
      docker \
      graphviz \
      shadow \
      ttf-opensans && \
    chmod +x composer.phar && \
    ./composer.phar install --optimize-autoloader --no-cache && \
    apk del --no-network curl && \
    rm composer.phar && \
    rm -rf /tmp/* && \
    rm -rf vendor/phpdocumentor/graphviz/tests/* && \
    rm -rf vendor/splitbrain/php-cli/tests/* && \
    rm -rf vendor/splitbrain/php-cli/examples/* && \
    rm vendor/splitbrain/php-cli/screenshot.png && \
    rm vendor/splitbrain/php-cli/screenshot2.png && \
    chmod +x /docker-graph/src/render && \
    chmod +x /docker-graph/bin/docker-entrypoint.sh && \
    addgroup dg && \
    adduser -D -G dg -s /bin/sh -h /input dg

VOLUME /input
WORKDIR /input
ENTRYPOINT ["/docker-graph/bin/docker-entrypoint.sh"]
CMD ["render"]