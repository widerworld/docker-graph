<?php

namespace DockerGraph\Models;

class DockerImage
{
    /** @param DockerContainer[] $containers */
    public function __construct(
        public string $digest       = '',
        public string $id           = '',
        public string $repository   = '',
        public string $tag          = '',
        public array  $containers   = [],
        public bool   $inUse        = false,
        public string $dockerFile   = '',
        public string $base         = '',
    ) {}
}
