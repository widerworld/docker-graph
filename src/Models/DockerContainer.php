<?php

namespace DockerGraph\Models;

class DockerContainer
{
    public function __construct(
        public string               $command          = '',
        public string               $id               = '',
        public string               $image            = '',
        public array                $labels           = [],
        public ?DockerComposeConfig $composeConfig    = null,
        public string               $localVolumes     = '',
        public array                $mounts           = [],
        public array                $names            = [],
        public array                $networks         = [],
        public array                $ports            = [],
        public bool                 $inUse            = true,
    ) {}
}
