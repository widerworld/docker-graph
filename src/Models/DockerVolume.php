<?php

namespace DockerGraph\Models;

class DockerVolume
{
    /** @param DockerContainer[] $containers */
    public function __construct(
        public string $driver       = '',
        public array  $labels       = [],
        public string $links        = '',
        public string $mountPoint   = '',
        public string $name         = '',
        public string $scope        = '',
        public ?array $containers   = null,
        public bool   $inUse        = false,
    ) {}
}
