<?php

namespace DockerGraph\Models;

class DockerNetwork
{
    /** @param DockerContainer[] $containers */
    public function __construct(
        public string $driver       = '',
        public string $id           = '',
        public bool   $internal     = false,
        public array  $labels       = [],
        public string $name         = '',
        public string $scope        = '',
        public array  $containers   = [],
        public bool   $inUse        = false,
    ) {}
}
