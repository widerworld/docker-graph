<?php

namespace DockerGraph\Models;

class DockerComposeConfig
{
    public function __construct(
        public array  $configFiles      = [],   // com.docker.compose.project.config_files
        public string $workingDirectory = '',   // com.docker.compose.project.working_dir
        public string $service          = '',   // com.docker.compose.service
        public string $project          = '',   // com.docker.compose.project
    ) {}
}
