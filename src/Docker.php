<?php

namespace DockerGraph;

use DockerGraph\Models\DockerImage;
use DockerGraph\Models\DockerVolume;
use DockerGraph\Models\DockerNetwork;
use DockerGraph\Models\DockerContainer;

class Docker
{
    /** @var DockerContainer[]  */
    public array $containers    = [];
    /** @var DockerImage[]  */
    public array $images        = [];
    /** @var DockerNetwork[]  */
    public array $networks      = [];
    /** @var DockerVolume[] */
    public array $volumes       = [];

    private const IMAGE      = 'image';
    private const CONTAINER  = 'container';
    private const VOLUME     = 'volume';
    private const NETWORK    = 'network';

    public static function create(): self
    {
        return new self();
    }

    /** @return DockerImage[] */
    public function listImages(): self
    {
        $images = [];
        foreach ($this->list(self::IMAGE) as $image) {
            $images[] = new DockerImage(
                digest:         $image['Digest'],
                id:             $image['ID'],
                repository:     $image['Repository'],
                tag:            $image['Tag'],
            );
        }
        $this->images = $images;
        return $this;
    }

    public function listContainers(): self
    {
        $containers = [];
        foreach ($this->list(self::CONTAINER) as $container) {
            $ports = str_replace('0.0.0.0:', '', $container['Ports']);
            $ports = str_replace(':::', '', $ports);
            $ports = explode(',', $ports);
            $ports = array_unique($ports);

            $containers[] = new DockerContainer(
                command:        $container['Command'],
                id:             $container['ID'],
                image:          $container['Image'],
                labels:         explode(',', $container['Labels']),
                localVolumes:   $container['LocalVolumes'],
                mounts:         explode(',', $container['Mounts']),
                names:          explode(',', $container['Names']),
                networks:       explode(',', $container['Networks']),
                ports:          $ports,
            );
        }
        $this->containers = $containers;
        return $this;
    }

    public function listVolumes(): self
    {
        $volumes = [];
        foreach ($this->list(self::VOLUME) as $volume) {
            $volumes[] = new DockerVolume(
                driver:     $volume['Driver'],
                labels:     explode(',', $volume['Labels']),
                links:      $volume['Links'],
                mountPoint: $volume['Mountpoint'],
                name:       $volume['Name'],
                scope:      $volume['Scope'],
            );
        }
        $this->volumes = $volumes;
        return $this;
    }

    public function listNetworks(): self
    {
        $networks = [];
        foreach ($this->list(self::NETWORK) as $network) {
            $networks[] = new DockerNetwork(
                driver:     $network['Driver'],
                id:         $network['ID'],
                internal:   (bool) $network['Internal'],
                labels:     explode(',', $network['Labels']),
                name:       $network['Name'],
                scope:      $network['Scope'],
            );
        }
        $this->networks = $networks;
        return $this;
    }

    private function list(string $type): array
    {
        switch ($type) {
            case self::IMAGE:
            case self::CONTAINER:
            case self::NETWORK:
                $type = escapeshellarg($type);
                $command = "docker $type list --format='{{json .}}' --no-trunc";
                break;
            case self::VOLUME:
                $type = escapeshellarg($type);
                $command = "docker $type list --format='{{json .}}'";
                break;
            default:
                return [];
        }

        exec($command, $output);

        $decoded = [];
        foreach ($output as $line) {
            $decoded[] = json_decode($line, true);
        }

        return $decoded;
    }
}
