<?php

namespace DockerGraph;

use Symfony\Component\Yaml\Yaml;
use DockerGraph\Models\DockerImage;
use DockerGraph\Models\DockerVolume;
use DockerGraph\Models\DockerNetwork;
use DockerGraph\Models\DockerContainer;
use DockerGraph\Models\DockerComposeConfig;

class DockerSnapshot
{
    /** @var DockerImage[]  */
    public array $containerImages   = [];
    /** @var DockerVolume[] */
    public array $volumes           = [];
    /** @var DockerNetwork[]  */
    public array $networks          = [];
    /** @var DockerImage[] */
    public array $baseImages        = [];

    /** @param  DockerContainer[] $containers */
    public function __construct(private array $containers) {}

    /** @param DockerContainer[] $containers */
    public static function create(array $containers): self
    {
        return new self($containers);
    }

    /** @param DockerImage[] $images */
    public function linkContainersToImages(array $images): self
    {
        foreach ($this->containers as $container) {
            foreach ($images as $image) {
                if ($container->image === $image->repository) {
                    $image->inUse        = true;
                    $image->containers[] = $container;
                }
            }
        }
        $this->containerImages = $images;
        return $this;
    }

    /** @param DockerContainer[] $containers */
    public function linkContainersToVolumes(array $volumes): self
    {
        foreach ($this->containers as $container) {
            foreach ($container->mounts as $containerMount) {
                foreach ($volumes as $volume) {
                    if ($containerMount == $volume->name) {
                        $volume->inUse = true;
                        $volume->containers[] = $container;
                    }
                }
            }
        }
        $this->volumes = $volumes;
        return $this;
    }

    /** @param DockerNetwork[] $networks */
    public function linkContainersToNetworks(array $networks):self
    {
        foreach ($this->containers as $container) {
            foreach ($networks as $network) {
                foreach ($container->networks as $containerNetwork) {
                    if ($containerNetwork === $network->name) {
                        $network->inUse = true;
                        $network->containers[] = $container;
                    }
                }
            }
        }
        $this->networks = $networks;
        return $this;
    }

    public function removeUnwantedImages(): self
    {
        $this->containerImages = array_values(array_filter($this->containerImages, function(DockerImage $image) {
            return
                $image->inUse &&
                !stristr($image->repository, 'docker-graph') &&
                $image->repository != 'composer' &&
                $image->repository != 'busybox';
        }));
        return $this;
    }

    public function removeUnwantedNetworks(): self
    {
        $this->networks = array_values(array_filter($this->networks, function(DockerNetwork $network) {
            return
                $network->inUse &&
                !stristr($network->name, 'docker-graph') &&
                $network->name !== 'none' &&
                $network->name !== 'bridge' &&
                $network->name !== 'host';
        }));
        return $this;
    }

    public function removeUnwantedVolumes(): self
    {
        $this->volumes = array_values(array_filter($this->volumes, function(DockerVolume $volume) {
            return $volume->inUse && (!$volume->inUse || !stristr($volume->name, 'docker-graph'));
        }));
        return $this;
    }

    public function addComposeConfigToContainers(): self
    {
        foreach ($this->containers as $container) {
            $configFiles = [];
            $workingDirectory = $service = $project = '';

            foreach ($container->labels as $label) {
                $parts = explode('=', $label);
                if ($parts[0] === 'com.docker.compose.project.config_files') {
                    $configFiles = explode(',', $parts[1]);
                }
                if ($parts[0] === 'com.docker.compose.project.working_dir') {
                    $workingDirectory = $parts[1];
                }
                if ($parts[0] === 'com.docker.compose.service') {
                    $service = $parts[1];
                }
                if ($parts[0] === 'com.docker.compose.project') {
                    $project = $parts[1];
                }
            }

            $container->composeConfig = new DockerComposeConfig(
                configFiles: $configFiles,
                workingDirectory: $workingDirectory,
                service: $service,
                project: $project,
            );
        }
        return $this;
    }

    public function createBaseImageList(): self
    {
        $baseImages = [];
        foreach ($this->containerImages as $image) {
            if (!empty($image->base)) {
                $baseImages[] = $image->base;
            }
        }
        $list = [];
        foreach (array_unique($baseImages) as $baseImage) {
            $list[] = new DockerImage(repository: $baseImage);
        }
        $this->baseImages = $list;
        return $this;
    }

    public function addBaseImagesToImages(): self
    {
        foreach ($this->containerImages as $image) {
            if ($image->dockerFile) {
                $parts = explode(' ', $image->dockerFile);
                $image->base  = explode(PHP_EOL, $parts[1])[0];
            }
        }
        return $this;
    }

    public function addDockerfilesToImages(): self
    {
        foreach ($this->containerImages as $image) {
            foreach ($this->containers as $container) {
                if ($container->image === $image->repository) {
                    $image->dockerFile = $this->extractDockerfileContents($container);
                }
            }
        }
        return $this;
    }

    private function extractDockerfileContents(DockerContainer $container): string
    {
        $composePathParts = explode('/', $container->composeConfig->workingDirectory);
        $composeFilePath = stristr($container->composeConfig->configFiles[0], 'docker-compose.yml')
            ? '/input/' . end($composePathParts) . '/docker-compose.yml' : null;
        $dockerfileContents = '';

        if (file_exists($composeFilePath)) {
            $composeFile = Yaml::parseFile($composeFilePath);
            if (isset($composeFile['services'][$container->composeConfig->service]['build'])) {
                $build = $composeFile['services'][$container->composeConfig->service]['build'];

                $dockerfileName = isset($build['dockerfile']) ? $build['dockerfile'] : 'Dockerfile';

                $filePath = isset($build['context']) ? $build['context'] . "/$dockerfileName" : $dockerfileName;
                $filePath = '/input/' . end($composePathParts) . str_replace('./.', '/.', $filePath);
                $filePath = str_replace('..', '', $filePath);

                if (file_exists($filePath)) {
                    $dockerfileContents = file_get_contents($filePath);
                } elseif ($relativePath = $this->searchForFileByRelativePath($filePath)) {
                    $dockerfileContents = file_get_contents($relativePath);
                }
            }
        }
        return $dockerfileContents;
    }

    private function searchForFileByRelativePath($filePath): false|string
    {
        if ($filePath === '../input/') {
            return false;
        }
        if (file_exists(__DIR__ . '/../' . $filePath)) {
            return $filePath;
        } else {
            $filePath = str_replace('../input/', '', $filePath);
            $filePath = explode('/', $filePath);
            unset($filePath[0]);
            $filePath =  '../input/' . implode('/', $filePath);
            return $this->searchForFileByRelativePath($filePath);
        }
    }
}
