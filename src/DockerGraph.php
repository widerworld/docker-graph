<?php

namespace DockerGraph;

use phpDocumentor\GraphViz\Edge;
use phpDocumentor\GraphViz\Node;
use phpDocumentor\GraphViz\Graph;
use DockerGraph\Models\DockerImage;
use DockerGraph\Models\DockerVolume;
use DockerGraph\Models\DockerNetwork;
use DockerGraph\Models\DockerContainer;

class DockerGraph
{
    private Graph $graph;
    private array $containerImageNodes = [];
    private array $containerNodes = [];

    public function __construct(private string $name)
    {
        $this->graph = Graph::create(strtolower(str_replace(' ', '_', $this->name)));
        $this->graph->setAttribute('label', "$this->name\n");
        $this->graph->setAttribute('labelloc', 't');
        $this->graph->setAttribute('labeljust', 'l');
        $this->graph->setAttribute('ratio', 'compress');
        $this->graph->setAttribute('size', '50');
        $this->graph->setAttribute('pad', '0.99');
        $this->graph->setAttribute('splines', 'spline');
        $this->graph->setAttribute('overlap', 'prism');
        $this->graph->setAttribute('overlap_scaling', '-123434');
        $this->graph->setAttribute('bgcolor', 'steelblue');
        $this->graph->setAttribute('fontname', 'Arial');
        $this->graph->setAttribute('fontcolor', 'white');
        $this->graph->setAttribute('fontsize', '36');
        $this->graph->setAttribute('rankdir', 'LR');
        $this->graph->setAttribute('ranksep', '1.2 equally');

        $date = Node::create('date');
        $this->setDefaults($date)
            ->setAttribute('label', date('r'))
            ->setAttribute('shape', 'rectangle')
            ->setAttribute('color', 'grey16')
            ->setAttribute('style', 'rounded, filled')
            ->setAttribute('fontcolor', 'white')
            ->setAttribute('height', '0.75');
        $this->graph->setNode($date);
    }

    public static function create(string $name): self
    {
        return new self($name);
    }

    /**
     * @param DockerImage[] $baseImages
     * @param DockerImage[] $containerImages
     */
    public function addImages(array $baseImages, array $containerImages): self
    {
        $rootImageGroup = Graph::create('cluster_root_images');
        $rootImageGroup->setAttribute('label', 'Images');
        $this->setDefaults($rootImageGroup);

        $this->graph->addGraph($rootImageGroup);

        $baseImageGroup = Graph::create('cluster_base_images');
        $baseImageGroup->setAttribute('color', 'invis');
        $baseImageGroup->setAttribute('rank', 'same');
        $baseImageGroup->setAttribute('style', 'rounded');

        $baseImageNodes = [];
        foreach ($baseImages as $baseImage) {
            $baseImageId = $baseImage->repository;

            $baseImageNodes[$baseImageId] = Node::create($baseImageId);
            $this->setDefaults($baseImageNodes[$baseImageId])
                 ->setAttribute('label', "$baseImageId\l")
                 ->setAttribute('shape', 'component');

            $baseImageGroup->setNode($baseImageNodes[$baseImageId]);
        }

        $rootImageGroup->addGraph($baseImageGroup);

        $containerImageGroup = Graph::create('cluster_container_images');
        $containerImageGroup->setAttribute('color', 'invis');
        $containerImageGroup->setAttribute('rank', 'same');
        $containerImageGroup->setAttribute('style', 'rounded');

        foreach ($containerImages as $containerImage) {
            $containerImageId = $containerImage->repository;

            $this->containerImageNodes[$containerImageId] = Node::create($containerImageId);
            $this->setDefaults($this->containerImageNodes[$containerImageId])
                 ->setAttribute('label', "$containerImageId\l")
                 ->setAttribute('shape', !empty($containerImage->base) ? 'tab' : 'component');

            $containerImageGroup->setNode($this->containerImageNodes[$containerImageId]);

            if (!empty($containerImage->base)) {
                $edge = Edge::create($baseImageNodes[$containerImage->base], $this->containerImageNodes[$containerImageId]);
                $this->setDefaults($edge)
                     ->setAttribute('weight', '500')
                     ->setAttribute('color', 'darkseagreen1');
                $this->graph->link($edge);
            }
        }

        $rootImageGroup->addGraph($containerImageGroup);

        return $this;
    }

    /** @param DockerNetwork[] $networks */
    public function addNetworks(array $networks): self
    {
        $groups = [];
        foreach ($networks as $networkId => $network) {
            $clusterBasename = 'cluster_network_' . $network->name;

            $groups[$networkId] = Graph::create($clusterBasename);
            $this->setDefaults($groups[$networkId])
                 ->setAttribute('label', "Containers on network: $network->name");

            $i = 0;
            $subGroup = Graph::create($clusterBasename . '_' . $i);
            $subGroup->setAttribute('color', 'invis');
            $subGroup->setAttribute('rank', 'same');
            foreach ($network->containers as $container) {
                $containerId = $container->names[0];

                $this->containerNodes[$containerId] = Node::create($containerId);
                $this->setDefaults($this->containerNodes[$containerId])
                     ->setAttribute('label', $this->containerLabel($container))
                     ->setAttribute('shape', 'rectangle')
                     ->setAttribute('fixedsize', 'true')
                     ->setAttribute('width', '10')
                     ->setAttribute('height', '1.75')
                     ->setAttribute('color', 'gray12')
                     ->setAttribute('style', 'filled, rounded');

                if ($i % 6 == 0) {
                    $subGroup = Graph::create($clusterBasename . '_' . ($i / 6));
                    $subGroup->setAttribute('color', 'invis');
                    $subGroup->setAttribute('rank', 'same');
                }

                if ($container->image != null && isset($this->containerImageNodes[$container->image])) {
                    $edge = Edge::create($this->containerImageNodes[$container->image], $this->containerNodes[$containerId]);
                    $this->setDefaults($edge)
                         ->setAttribute('weight', '600')
                         ->setAttribute('color', 'orange');
                    $this->graph->link($edge);
                }

                $subGroup->setNode($this->containerNodes[$containerId]);
                $groups[$networkId]->addGraph($subGroup);

                $i++;
            }

            $this->graph->addGraph($groups[$networkId]);
        }

        return $this;
    }

    /** @param DockerVolume[] $volumes */
    public function addVolumes(array $volumes): self
    {
        $group = Graph::create('cluster_volumes')->setAttribute('label', 'Volumes');
        $this->setDefaults($group);

        $nodes = [];
        foreach ($volumes as $volume) {
            $nodes[$volume->name] = Node::create($volume->name);
            $this->setDefaults($nodes[$volume->name])
                 ->setAttribute('label', $this->volumeLabel($volume->name))
                 ->setAttribute('shape', 'cylinder');

            $group->setNode($nodes[$volume->name]);

            foreach ($volume->containers as $container) {
                $containerId = $container->names[0];
                if (!isset($this->containerNodes[$containerId])) {
                    break;
                }
                $edge = Edge::create($nodes[$volume->name], $this->containerNodes[$containerId]);
                $this->setDefaults($edge)
                     ->setAttribute('dir', 'both')
                     ->setAttribute('arrowhead', 'dot')
                     ->setAttribute('arrowtail', 'dot')
                     ->setAttribute('weight', '300')
                     ->setAttribute('color', 'yellow1');
                $this->graph->link($edge);
            }
        }

        $this->graph->addGraph($group);

        return $this;
    }

    public function render(?string $format='', ?string $filename='', $dot=false): self
    {
        $format = strtolower($format) === 'png' ? 'png' : 'svg';
        $filename = $filename ?: strtolower(str_replace(' ', '_', $this->name));
        $this->graph->export($format, "/input/$filename.$format");
        if ($dot) {
            file_put_contents("/input/$filename.dot", (string) $this->graph);
        }
        return $this;
    }

    private function setDefaults(Graph|Node|Edge $graphObject): Graph|Node|Edge
    {
        switch ($graphObject::class) {
            case Graph::class:
                return $graphObject->setAttribute('labeljust', 'l')
                    ->setAttribute('fontname', 'Arial')
                    ->setAttribute('color', 'invis')
                    ->setAttribute('fontcolor', 'white')
                    ->setAttribute('fontsize', '18')
                    ->setAttribute('bgcolor', 'steelblue4')
                    ->setAttribute('rank', 'same')
                    ->setAttribute('style', 'rounded');
            case Node::class:
                return $graphObject->setAttribute('fontname', 'Arial')
                    ->setAttribute('color', 'white')
                    ->setAttribute('width', '4')
                    ->setAttribute('height', '1.25')
                    ->setAttribute('fontcolor', 'white');
            case Edge::class:
                return $graphObject->setAttribute('penwidth', '2');
            default:
                return $this->setDefaults(Graph::create());
        }
    }

    private function volumeLabel(string $name): string
    {
        return strlen($name) > 50 ? substr($name, 0, 25) . '...' : $name;
    }

    private function containerLabel(DockerContainer $container): string
    {
        $parts = explode('/', $container->composeConfig->workingDirectory);

        $path = stristr($container->composeConfig->configFiles[0], 'docker-compose.yml')
            ? end($parts) . '/docker-compose.yml'
            : $container->composeConfig->configFiles[0];

        $label  = 'Name: ' . $container->names[0] . '\l\n';
        $label .= 'Ports: ' . implode(',', $container->ports) . '\l\n';
        $label .= "Docker Compose ($path):\l\\n";
        $label .= '--> Project: ' . $container->composeConfig->project . ' | Service: ' . $container->composeConfig->service .'\l';

        return $label;
    }
}
