# Docker Graph - Development

## Instructions

### Before you start

* Run `docker build --build-arg with_xdebug=1 --file dev.Dockerfile --tag docker-graph .`

An Xdebug build step is included in the Dockerfile, but is not run by default. Enable/disable it with `--build-arg with_xdebug=1|0`

### Using Docker Compose for development:

1. Run `docker-compose run --rm docker-graph-dev`
2. On the container, run (for example) `./composer.phar install`

### Testing Changes

1. Change to your projects' parent directory (or replace `$PWD` with the project parent's absolute path in the command below)
2. Run `docker run --rm -v $PWD/docker-graph:/docker-graph -v $PWD:/input -v /var/run/docker.sock:/var/run/docker.sock docker-graph-dev render -f test.png -d`

Output should match: 

```
Docker Graph file created: test.png
DOT file created: test.dot
```

Changes merged to `origin/main` will get built on Docker Hub, so make sure they work! 