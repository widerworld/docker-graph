# Docker Graph

## Overview

Docker Graph uses [Graphviz](https://graphviz.org/) to create a [directed graph](https://en.wikipedia.org/wiki/Directed_graph) of Docker images, volumes and containers (grouped by network). It traces each container back to its base image by analysing the output of the `docker list` command, and parsing the contents of Docker Compose files to locate and parse Dockerfiles.

By default, graphs are rendered in SVG format, but this can be altered (see the Help command below).

In order to inspect your environment, it runs Docker inside the container.

## Instructions

### Before you Start

1. Your host user must be in the `docker` group \*
2. The containers you wish to analyse must be running
3. Docker Graph assumes your Docker-based projects are contained within subdirectories of a single directory with `docker-compose.yml` files in the root of each

_\* To communicate with the host's Docker daemon, Docker Graph requires the host's `/var/run/docker.sock` to be mounted in the container, therefore it's **unwise** to use this outside of a local / development environment_

### Commands

#### Rendering

* Execute this from the **parent directory containing your Docker projects**, or replace `$PWD` with the absolute path to that directory (otherwise Docker Graph will be unable to find base images using your projects' Docker Compose files)
* Creates an output file ( `docker_graph.svg`) at the host's `/input` mount point

##### Linux
`docker run --rm -v $PWD:/input -v /var/run/docker.sock:/var/run/docker.sock ihasco/docker-graph render`

##### WSL2
`docker run --rm -v $PWD:/input -v //var/run/docker.sock:/var/run/docker.sock ihasco/docker-graph render`

#### Help

To see the list of `render` options

`docker run --rm ihasco/docker-graph render --help`

## Sample

![Directed graph created using docker Graph](https://i.ibb.co/RzHvTBG/docker-graph.png)


